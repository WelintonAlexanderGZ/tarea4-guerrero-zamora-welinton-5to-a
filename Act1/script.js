meses = [
  "Enero",
  "Febrero",
  "Marzo",
  "Abril",
  "Mayo",
  "Junio",
  "Julio",
  "Agosto",
  "Septiembre",
  "Octubre",
  "Noviembre",
  "Diciembre",
];

function elige() {
  var lista = document.getElementById("num");
  if (lista.selectedIndex == null || lista.selectedIndex == 0) {
    alert("Numero no seleccionado; \nDebe elegir un numero de la lista.");
    return false;
  } else {
    seleccion = lista.selectedIndex - 1;
    alert(meses[seleccion]);
  }
}

function escribir2() {
  for (i = 0; i < meses.length; i++) {
    var text = document.createTextNode(meses[i]+'\n');
    document.getElementById("cont3").appendChild(text);
  }
  setTimeout(() => {
    // Eliminando todos los hijos de un elemento
    var ele = document.getElementById("cont3");
    while (ele.firstChild) {
      ele.removeChild(ele.firstChild);
    }
  }, 6000);
}