let table = document.createElement("table");
let thead = document.createElement("thead");
let tbody = document.createElement("tbody");

table.appendChild(thead);
table.appendChild(tbody);

// Agregar la tabla completa a la etiqueta del cuerpo
document.getElementById("body").appendChild(table);

//Objeto definido: Producto_alimenticio con sus propiedades código, nombre y precio
function Producto_alimenticio(Codigo, Nombre, Precio) {
  this.Codigo = Codigo;
  this.Nombre = Nombre;
  this.Precio = Precio;
}

// Crear y agregar datos a la primera fila de la tabla
let row_1 = document.createElement("tr");
let heading_1 = document.createElement("th");
heading_1.innerHTML = "Codigo";
let heading_2 = document.createElement("th");
heading_2.innerHTML = "Nombre";
let heading_3 = document.createElement("th");
heading_3.innerHTML = "Precio U.";

row_1.appendChild(heading_1);
row_1.appendChild(heading_2);
row_1.appendChild(heading_3);
thead.appendChild(row_1);

//Instancias del objeto y guárdadas en un array
var producto1 = new Producto_alimenticio(1001, "Manzana", "$ 0.80");
var producto2 = new Producto_alimenticio(1002, "Frutilla", "$ 0.50");
var producto3 = new Producto_alimenticio(1003, "Mora", "$ 0.50");

//Bucle for
var array = [producto1, producto2, producto3];
for (i = 0; i < array.length; i++) {
  imprimeDatos(array[i]);
}

//Método imprimeDatos, el cual escribe por pantalla los valores de sus propiedades
function imprimeDatos(array) {
  // Crear y agregar datos a las filas de la tabla
  let row_2 = document.createElement("tr");
  let row_2_data_1 = document.createElement("td");
  row_2_data_1.innerHTML = array.Codigo;
  let row_2_data_2 = document.createElement("td");
  row_2_data_2.innerHTML = array.Nombre;
  let row_2_data_3 = document.createElement("td");
  row_2_data_3.innerHTML = array.Precio;
  row_2.appendChild(row_2_data_1);
  row_2.appendChild(row_2_data_2);
  row_2.appendChild(row_2_data_3);
  tbody.appendChild(row_2);
}